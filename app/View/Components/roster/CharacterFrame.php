<?php

namespace App\View\Components\roster;

use Illuminate\View\Component;

class CharacterFrame extends Component
{
    public $character;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($character = [])
    {
        $this->character = $character;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.roster.character-frame');
    }
}
