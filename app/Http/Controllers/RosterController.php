<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RosterController extends Controller
{

    public function index()
    {
        $db = ["characters" => [
            [
                "basic" => [
                    "Name"=>"Some name",
                    "Background"=>"Some background",
                    "Career"=>"Some career",
                    "Interest"=>"Some interest",
                    "Faction"=>"Some faction",
                    "Muse"=>"Some muse"
                ],
                "aptitude" => [
                    "COG" => 15,
                    "INT" => 15,
                    "REF" => 15,
                    "SAV" => 15,
                    "SOM" => 15,
                    "WIL" => 15
                ]
            ],
            [
                "basic" => [
                    "Name"=>"Some name",
                    "Background"=>"Some background",
                    "Career"=>"Some career",
                    "Interest"=>"Some interest",
                    "Faction"=>"Some faction",
                    "Muse"=>"Some muse"
                ],
                "aptitude" => [
                    "COG" => 15,
                    "INT" => 15,
                    "REF" => 15,
                    "SAV" => 15,
                    "SOM" => 15,
                    "WIL" => 15
                ]
            ],
            [
                "basic" => [
                    "Name"=>"Some name",
                    "Background"=>"Some background",
                    "Career"=>"Some career",
                    "Interest"=>"Some interest",
                    "Faction"=>"Some faction",
                    "Muse"=>"Some muse"
                ],
                "aptitude" => [
                    "COG" => 15,
                    "INT" => 15,
                    "REF" => 15,
                    "SAV" => 15,
                    "SOM" => 15,
                    "WIL" => 15
                ]
            ],
            [
                "basic" => [
                    "Name"=>"Some name",
                    "Background"=>"Some background",
                    "Career"=>"Some career",
                    "Interest"=>"Some interest",
                    "Faction"=>"Some faction",
                    "Muse"=>"Some muse"
                ],
                "aptitude" => [
                    "COG" => 15,
                    "INT" => 15,
                    "REF" => 15,
                    "SAV" => 15,
                    "SOM" => 15,
                    "WIL" => 15
                ]
            ],
            [
                "basic" => [
                    "Name"=>"Some name",
                    "Background"=>"Some background",
                    "Career"=>"Some career",
                    "Interest"=>"Some interest",
                    "Faction"=>"Some faction",
                    "Muse"=>"Some muse"
                ],
                "aptitude" => [
                    "COG" => 15,
                    "INT" => 15,
                    "REF" => 15,
                    "SAV" => 15,
                    "SOM" => 15,
                    "WIL" => 15
                ]
            ],
        ]];
        return view('roster', $db);
    }
}
