<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Character;


class CharacterGenerator extends Component
{
    public Character $character;

    public $appPageTitle = "Character Generator";

    public function mount(Character $character)
    {
        $this->character = $character;
        $this->character->name = "test";
        dd($this->character);
    }

    public function render()
    {
        return view('livewire.character-generator');
    }
}
