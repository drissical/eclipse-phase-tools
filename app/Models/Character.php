<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    use HasFactory;

    protected $fillable = array(
        "name", "alias", "language",
        "background", "career",
        "interest", "faction",
        "gender", "sex", "age"
    );

    public string $name;
    public string $alias;
    public string $language;
    public string $background;
    public string $career;
    public string $interest;
    public string $faction;
    public string $gender;
    public string $sex;
    public string $age;

    public function __construct()
    {
        $this->name = "";
        $this->alias = "";
        $this->language = "";
        $this->background = "";
        $this->career = "";
        $this->interest = "";
        $this->faction = "";
        $this->gender = "";
        $this->sex = "";
        $this->age = "";
    }
}
