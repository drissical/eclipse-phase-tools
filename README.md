# PHP Application
## About
PHP Application

## Technologies
Using the TALL stack to create this which is composed of **T**ailwindCSS, **A**lpineJS, **L**laravel, and **L**ivewire.

* [TailwindCSS](https://tailwindcss.com/) - Utility first CSS framework.
* [AlpineJS](https://alpinejs.dev/) - lightweight JavaScript framework
* [Laravel](https://laravel.com/) - Web application framework
* [Livewire](https://laravel-livewire.com/) - Full-stack framework for Laravel

## Development
### Requirements
This project uses Docker for creating development containers as well has composer (PHP) and npm (nodeJS) for building the development and application environments up.

Make sure that your initial environment (before docker) has Docker, PHP8, and NodeJS installed on it. Setup might complain about missing dependencies and they might not all be listed.
#### Additional Libraries
* php-xml
* php-curl
### Build Setup
Currently building this project is subject to some default setups.
```bash
# Clone the project
git clone git@gitlab.com:drissical/
cd 

# Pull down libraries
composer install
npm install

# Starting dev
./vendor/bin/sail up -d
npm run dev
```
## Notes
You can simplify the sail command with an alias assuming bash, other shells may differ.
```bash
# Place this in to your .bashrc file
alias sail='[ -f sail ] && sh sail || sh vendor/bin/sail'
```