<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('characters', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('alias');
            $table->string('language');
            $table->string('background');
            $table->string('career');
            $table->string('interest');
            $table->string('faction');
            $table->string('gender');
            $table->string('sex');
            $table->string('age');
            // $table->integer('COG');
            // $table->integer('INT');
            // $table->integer('REF');
            // $table->integer('SAV');
            // $table->integer('SOM');
            // $table->integer('WIL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('characters');
    }
};
