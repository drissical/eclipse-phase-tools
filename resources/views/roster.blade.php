@extends('layouts.main')

@section('title', 'Character Roster')

@section('appPageTitle')
Character Roster
@endsection

@section('appPageDescription')
Page for characters.
@endsection

@section('appPageControls')
    <a href="{{url('character-generator')}}"><div class="btnNewCharacter elementButton">New Character</div></a>
@endsection

@section('appPageContent')
    <div class="character-roster pageRoster">
        @foreach ($characters as $character)
            <x-roster.character-frame :character="$character" />
        @endforeach
    </div>
@endsection


