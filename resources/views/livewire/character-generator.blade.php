<div>
    <div>EGO DETAILS</div>
    <div>Name</div> {{ $character->name }}
    <div>Aliases</div> {{ $character->alias }}
    <div>Languages</div>
    <div>Background</div>
    <div>Career</div>
    <div>Interest</div>
    <div>Faction</div>
    <div>Gender</div> {{ $character->gender }}
    <div>Sex</div>
    <div>Age</div>

    <div>EGO APTITUDES & STATS</div>
    <div>Cog</div>
    <div>Int</div>
    <div>Ref</div>
    <div>Sav</div>
    <div>Som</div>
    <div>Wil</div>
</div>