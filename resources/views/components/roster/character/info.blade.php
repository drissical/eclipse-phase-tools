<div class="character-info-{{$type}}">
    @foreach ($data as $name=>$value)
        <x-roster.character.info-pair :type="$type" :name="$name" :value="$value" />
    @endforeach
</div>