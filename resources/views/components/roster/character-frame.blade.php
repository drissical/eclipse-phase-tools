<div class="character-frame">
    <x-roster.character.info type="basic" :data="$character['basic']" />
    <x-roster.character.info type="aptitude" :data="$character['aptitude']" />
</div>