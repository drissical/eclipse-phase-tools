<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title', config('app.name'))</title>
		<link rel="shortcut icon" href="{{ url(asset('favicon.ico')) }}">
        @vite(['resources/sass/app.scss', 'resources/js/app.js'])
        @livewireStyles
        @livewireScripts
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
        <div class="main-title">
            Eclipse Phase Tools
        </div>
        <div class="main-content">
            <div class="appPage">
                <div class="appPage-header">
                    <div class="sub-header">
                        <div class="appPage-title">@yield('appPageTitle','')</div>
                        <div class="appPage-description">@yield('appPageDescription', '')</div>
                    </div>
                    <div class="appPage-controls">
                        @yield('appPageControls', '')
                    </div>
                </div>
                <div class="appPage-content">
                    @yield('appPageContent', '')
                </div>
            </div>
        </div>
    </body>
</html>