@extends('layouts.main')

@isset($title)
    @section('title')
        {{ $title}}
    @endsection
@endisset

@isset($appPageTitle)
    @section('appPageTitle')
        {{ $appPageTitle }}
    @endsection
@endisset

@isset($appPageDescription)
    @section('appPageDescription')
        {{ $appPageDescription }}
    @endsection
@endisset

@isset($appPageControls)
    @section('appPageControls')
        {{ $appPageControls }}
    @endsection
@endisset

@section('appPageContent')
    <div class="character-roster pageRoster">
        @isset($slot)
            {{ $slot }}
        @endisset
    </div>
@endsection